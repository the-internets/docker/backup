#! /bin/bash
# https://github.com/iainmckay/docker-mysql-backup
# https://github.com/joshbenner/docker-mysql-dump

if [ ! -n "$MYSQL_BACKUP_DIR" ] || [ ! -n "$MYSQL_USER" ] || [ ! -n "$MYSQL_HOST" ] || [ ! -n "$MYSQL_PORT" ] || [ ! -n "$MYSQL_PASSWORD" ]; then
  echo "Missing mysql args, skipping backup..."
  exit 1
fi

# remove older backup than 10 days
echo "Cleaning 10 days+ backups..."
find ${MYSQL_BACKUP_DIR}/* -maxdepth 1 -type d -ctime +10 -exec rm -rf {} +

# dump mysql dbs
mkdir -p $MYSQL_BACKUP_DIR

MYSQL_CONN="--user=$MYSQL_USER --host=$MYSQL_HOST --port=$MYSQL_PORT --password=$MYSQL_PASSWORD"

if [ -n "$2" ]; then
    databases=$2
else
    echo "Finding databases..."
    databases=$(docker exec mysql sh -c "mysql ${MYSQL_CONN} -N -e 'SHOW DATABASES;' | grep -Ev '(information_schema|performance_schema)'")
fi

if [ -n "$DUMP_TIMESTAMP" ]; then
    timestamp=`date +%Y-%m-%d.%H%M%S`
    path="${MYSQL_BACKUP_DIR}/${timestamp}/"
    mkdir -p $path
else
    path="${MYSQL_BACKUP_DIR}/"
fi

for db in $databases; do
    echo "Dumping: $db..."
    docker exec mysql sh -c "mysqldump --force --events --opt ${MYSQL_CONN} --databases $db" | gzip > "${path}${db}.gz"
done

# send to remote
# if [ -n "$SSH_USER" ] || [ -n "$SSH_PATH" ] && [ -n "$SSH_HOST" ] && [ -s "/root/.ssh/id_rsa" ]; then
# 	if [ ! -n $SSH_PORT ]; then
# 		SSH_PORT=22
# 	fi

# 	rsync -av --delete -e "ssh -p $SSH_PORT -i /root/.ssh/id_rsa" $MYSQL_BACKUP_DIR ${SSH_USER}@${SSH_HOST}:${SSH_PATH}
# 	# duplicity --no-encryption --rsync-options='-e "ssh -i /root/.ssh/id_rsa -p $SSH_PORT"' $MYSQL_BACKUP_DIR rsync://${SSH_USER}@${SSH_HOST}/${SSH_PATH}/mysql
# 	# duplicity --allow-source-mismatch --full-if-older-than 1M --volsize $DUPLICITY_VOL_SIZE --rsync-options='-e "ssh -i /root/.ssh/id_rsa -p 2222"' /var/lib/mysql/dump rsync://${SSH_USER}@${SSH_HOST}/${SSH_PATH}/mysql

# 	# need restoration?
# 	# duplicity --allow-source-mismatch --rsync-options='-e "ssh -i /root/.ssh/id_rsa -p 2222"' rsync://backup@fqdn.cheron.works//volume1/server-backup/production01/mysql/ /restoration-folder
# fi

currently_backing_up=$(ps -ef | grep duplicity | grep python | wc -l)

if [ $currently_backing_up -eq 0 ]; then
	duplicity remove-older-than \
	--s3-endpoint-url "${SCW_ENDPOINT_URL}" \
	--s3-region-name "${SCW_REGION}" \
	${KEEP_BACKUP_TIME} --force "${SCW_BUCKET}/mysql" >> ${LOGFILE_RECENT} 2>&1

	# duplicity >= 0.8.23
	# determine S3_ENDPOINT_URL for scaleway
	S3_ENDPOINT_URL="https://s3.${S3_REGION_NAME}.scw.cloud"

	duplicity \
	incr --full-if-older-than ${FULL_BACKUP_TIME} \
	--asynchronous-upload \
	--s3-endpoint-url "${SCW_ENDPOINT_URL}" \
	--s3-region-name "${SCW_REGION}" \
	--encrypt-key=${GPG_FINGERPRINT} \
	--sign-key=${GPG_FINGERPRINT} \
	$MYSQL_BACKUP_DIR "${SCW_BUCKET}/mysql"
fi