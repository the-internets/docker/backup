#!/bin/bash

# Backup /var/www'
# if [ -n "$SSH_USER" ] && [ -n "$SSH_PATH" ] && [ -n "$SSH_HOST" ] && [ -s "/root/.ssh/id_rsa" ]; then

# 	if [ ! -n $SSH_PORT ]; then
# 		SSH_PORT=22
# 	fi

# 	echo "Syncing..."
# 	rsync -av --delete -e "ssh -p $SSH_PORT -i /root/.ssh/id_rsa" /var/www ${SSH_USER}@${SSH_HOST}:${SSH_PATH}

# 	# duplicity --no-encryption --rsync-options='-e "ssh -i /root/.ssh/id_rsa -p $SSH_PORT"' \
# 	# 	/var/www \
# 	# 	rsync://${SSH_USER}@${SSH_HOST}/${SSH_PATH}/www

# 	# duplicity --allow-source-mismatch --volsize $DUPLICITY_VOL_SIZE --rsync-options='-e "ssh -i /root/.ssh/id_rsa -p ${SSH_PORT}"' \
# 	# 	/var/www \
# 	# 	rsync://${SSH_USER}@${SSH_HOST}/${SSH_PATH}/www

# 	# need restoration?
# 	# duplicity --allow-source-mismatch --rsync-options='-e "ssh -i /id_rsa -p 2222"' rsync://backup@fqdn.cheron.works//volume1/server-backup/production01/www /restoration-folder

# 	# need to test rsync?
# 	# touch test.txt
# 	# rsync -av -e 'ssh -p 2222 -i /root/.ssh/id_rsa' $(pwd)/test.txt backup@192.168.1.15:/destination/
# fi

# https://www.scaleway.com/en/docs/tutorials/backup-dedicated-server-s3-duplicity/
currently_backing_up=$(ps -ef | grep duplicity | grep python | wc -l)

if [ $currently_backing_up -eq 0 ]; then
	duplicity remove-older-than \
	--s3-endpoint-url "${SCW_ENDPOINT_URL}" \
	--s3-region-name "${SCW_REGION}" \
	${KEEP_BACKUP_TIME} --force "${SCW_BUCKET}/www"

	# duplicity >= 0.8.23
	# determine S3_ENDPOINT_URL for scaleway
	S3_ENDPOINT_URL="https://s3.${S3_REGION_NAME}.scw.cloud"

	# backup, add --s3-use-glacier \ if needed
	duplicity \
	incr --full-if-older-than ${FULL_BACKUP_TIME} \
	--volsize 500 \
	--asynchronous-upload \
	--s3-endpoint-url "${SCW_ENDPOINT_URL}" \
	--s3-region-name "${SCW_REGION}" \
	--encrypt-key=${GPG_FINGERPRINT} \
	--sign-key=${GPG_FINGERPRINT} \
	/var/www "${SCW_BUCKET}/www"
fi
