#!/bin/bash
# https://www.scaleway.com/en/docs/tutorials/backup-dedicated-server-s3-duplicity/

if [ $# -lt 2 ]; then
	echo -e "Usage $0 <time or delta> [file to restore] <restore to>
	Exemple:
	\tsh $0 2018-7-21 /var/www/recovery  ## recovers * from closest backup to date
	\tsh $0 0D secret /var/www/recovery  ## recovers most recent file nammed 'secret'";
exit; fi

if [ $# -eq 2 ]; then
	duplicity \
	--s3-endpoint-url ${SCW_ENDPOINT_URL} \
	--s3-region-name ${SCW_REGION} \
	--time $1 \
	"${SCW_BUCKET}/www2" $2
fi

if [ $# -eq 3 ]; then
	duplicity \
	--s3-endpoint-url ${SCW_ENDPOINT_URL} \
	--s3-region-name ${SCW_REGION} \
	--time $1 \
	--file-to-restore $2 \
	"${SCW_BUCKET}/www2" $2
fi