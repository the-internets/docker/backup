# https://hub.docker.com/r/wernight/duplicity/~/dockerfile/

FROM alpine:3.16

RUN apk add --update duplicity openssh openssl py-pip py3-boto3 docker rsync findutils vim \
 && pip install pydrive==1.0.1 \
 && apk del --purge py-pip \
 && rm /var/cache/apk/* \
 # && adduser -D -u 1896 duplicity \
 && mkdir -p /home/duplicity/.cache/duplicity \
 && mkdir -p /home/duplicity/.gnupg \
 && chmod -R go+rwx /home/duplicity/

# set timezone
RUN apk add --update tzdata \
 && cp /usr/share/zoneinfo/Europe/Paris /etc/localtime \
 && echo "Europe/Paris" > /etc/timezone \
 && apk del tzdata

# add cron jobs
RUN (crontab -l ; echo "00 3 * * * sh /home/scripts/backup-mysql.sh") | sort - | uniq - | crontab - \
 && (crontab -l ; echo "30 3 * * * sh /home/scripts/backup-www.sh") | sort - | uniq - | crontab -

# ENV HOME=/home/duplicity

VOLUME /home/duplicity/.cache/duplicity
VOLUME /home/duplicity/.gnupg

COPY scripts /home/scripts

# USER duplicity

CMD ["duplicity --archive-dir=/home/duplicity/.cache/duplicity"]